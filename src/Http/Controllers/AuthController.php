<?php
namespace Inmovsoftware\LoginApi\Http\Controllers;

use App\Http\Controllers\Controller;
use Inmovsoftware\LoginApi\Models\Userlogin as IT_User;
use Inmovsoftware\UserApi\Models\User as IT_User_Model;
use Illuminate\Http\Request;
use JWTAuth;
use Carbon\Carbon;

class AuthController extends Controller
{


    public function login(Request $request)
    {
        $credentials = request(['login', 'password']);
        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized. Check Credentials']);
        }

        $Auth_user = auth('api')->user();
        $MyUser = IT_User::find($Auth_user->id)->toArray();
        $response = array();

        $user = IT_User_Model::find($MyUser["id"]);
        if(is_null($MyUser["first_login"])){
            $user->first_login = Carbon::now();
        }
            $user->last_login = Carbon::now();
        $user->save();

        $response = $user;
        $response["expires_in"] = auth('api')->factory()->getTTL() * 20160;
        $response["accessToken"] = $token;
        $response["refreshToken"] = $token;

        return response()->json($response);

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $Auth_user = auth('api')->user();
        $token = $request->bearerToken();
        $MyUser = IT_User::find($Auth_user->id)->toArray();
        $response = array();
        $response['user']=$MyUser;
        $response['token'] = $token;

        return response()->json($response);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $token = $request->bearerToken();
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        $old_token = $request->bearerToken();
        $new_token = auth('api')->refresh();
        $response = $this->respondWithToken($new_token);
        return $response;
    }

    public function invalidate()
    {
        #auth('api')->invalidate();
        // Pass true as the first param to force the token to be blacklisted "forever".
        auth('api')->invalidate(true);

        return response()->json(['message' => 'Token Invalidated successully.']);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 20160
        ]);
    }
}
