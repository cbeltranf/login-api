<?php
use Illuminate\Http\Request;
Route::group([
    'middleware' => ['api'],
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Inmovsoftware\LoginApi\Http\Controllers\AuthController@login');
    Route::get('logout', 'Inmovsoftware\LoginApi\Http\Controllers\AuthController@logout');
});

Route::middleware(['api', 'jwt'])->group(function () {
    Route::group([
        'prefix' => 'auth'
    ], function () {
        Route::get('me', 'Inmovsoftware\LoginApi\Http\Controllers\AuthController@me');
        Route::get('refresh', 'Inmovsoftware\LoginApi\Http\Controllers\AuthController@refresh');
        Route::get('invalidate', 'Inmovsoftware\LoginApi\Http\Controllers\AuthController@invalidate');
        });
});

