<?php
namespace Inmovsoftware\LoginApi\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Database\QueryException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Illuminate\Auth\AuthenticationException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class ErrorHandler extends ExceptionHandler
{
 /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
//    public function render($request, Exception $exception)
//       // {
//       //     return parent::render($request, $exception);
//       // }


//   public function render($request, Exception $exception)
//    {
//        return parent::render($request, $exception);
//    }


    public function render($request, Exception $exception)
    {

        if ($exception instanceof TokenInvalidException) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'message' => 'Token Invalid',
                    ]
                ],
                401
            );
        }


        if ($exception instanceof TokenExpiredException) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'message' => 'Token Expired',
                    ]
                ],
                401
            );
        }


        if ($exception instanceof JWTException) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'message' => 'Token Problem. JWT',
                        'more' => $exception->getMessage()
                    ]
                ],
                401
            );
        }


        if ($exception instanceof AuthenticationException) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'message' => 'Unauthenticated',
                        'more' => $exception->getMessage()
                    ]
                ],
                401
            );
        }

        if ($exception instanceof RouteNotFoundException) {

            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'message' => 'Route Not Valid',
                        'more' => $exception->getMessage()
                    ]
                ],
                401
            );
        }

        if ($exception instanceof ModelNotFoundException ) {

            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'message' => 'Element Not Found',
                        'more' => $exception->getMessage()
                    ]
                ],
                500
            );
        }


        if ($exception instanceof QueryException ) {

            return response()->json(
                [
                    'errors' => [
                        'status' => 502,
                        'message' => 'Restriction detected in database',
                        'more' => $exception->getMessage()
                    ]
                ],
                500
            );
        }



        return parent::render($request, $exception);
    }

}
